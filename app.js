var app = angular.module('cwc', ['ngRoute']).run(function ($rootScope, $interval, $location, $window) {

    //show numberpad boolean
    $rootScope.showNumpad = false;

    //show summary boolean
    $rootScope.summaryShow = false;

    //time and date
    $rootScope.timeAndDate = new Date();

    //date/time
    $interval(function () {
        $rootScope.timeAndDate = new Date();
    }, 1000);

    //run time
    $rootScope.startTime = 0;
    $rootScope.runTime = $interval(function () {
        $rootScope.startTime = $rootScope.startTime + 1000;
    }, 1000);

    //current tab
    $rootScope.currentTab = null;
    $rootScope.currentTabCount = 0;
    $rootScope.tabs = ["prepare", "run", "complete"];

    //current step
    $rootScope.steps = null;
    $rootScope.currentStep = 0;

    //next step
    $rootScope.nextStep = function () {
        if ($rootScope.currentStep + 1 === $rootScope.steps[$rootScope.currentTab].length) {
            $rootScope.currentTabCount++;
            $location.path('/' + $rootScope.tabs[$rootScope.currentTabCount]);
            $rootScope.currentTab = $rootScope.tabs[$rootScope.currentTabCount];
            $rootScope.currentStep = 0;
            return;
        }
        $rootScope.currentStep++;
    };

    //login/out
    $rootScope.loginOut = "login";
    $rootScope.loggedIn = false;
    $rootScope.loginClick = function () {

        //if not logged in
        if (!$rootScope.loggedIn) {
            $rootScope.userInfo.userName = document.getElementById("userInfo").value;
            $rootScope.loginOut = "log out";
            $rootScope.loggedIn = true;
            $rootScope.currentTab = $rootScope.tabs[$rootScope.currentTabCount];
            $location.path('/prepare');
        } else {
            $rootScope.userInfo.userName = '';
            $rootScope.loginOut = "login";
            $rootScope.loggedIn = false;
            $rootScope.currentTab = null;
            $rootScope.logout();
        }

    };

    //logout
    $rootScope.logout = function () {
        $location.path('/home');
        $rootScope.$on("$locationChangeSuccess", function () {
            $window.location.reload();
        });
    };

    //steps
    $rootScope.steps = {
        prepare: ["Calibrate System", "Enter Data", "Scan Set", "Load Set", "Scan and Connect Cryoprotectant", "Load Check", "Connect Input Bag", "Start Confirmation"],
        run: ["Move Cells to ISB", "Scan and Connect Cryoprotectant", "Prime Cryoprotectant", "Fill Bags"],
        complete: ["Procedure Summary", "Prepare Unload"]
    };

    //username/password
    $rootScope.userInfo = {
        userName: null,
        pw: null
    };

    //master object to store data values
    $rootScope.masterObj = {
        db1: null,
        db2: null,
        db3: null,
        qcb: null,
        cn: null,
        ln: null,
        sn: null,
        cp: null,
        ctb: null
    };
});

app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider.
        when('/home', {
            templateUrl: 'views/home.html',
            controller: 'home'
        }).when('/prepare', {
            templateUrl: 'views/prepare.html',
            controller: 'prepare'
        }).when('/run', {
            templateUrl: 'views/run.html',
            controller: 'run'
        }).when('/complete', {
            templateUrl: 'views/complete.html',
            controller: 'complete'
        }).otherwise({
            redirectTo: '/home'
        });
  }]);