app.controller('prepare', function ($scope) {

    //booleans for advance
    $scope.cal0g = false;
    $scope.cal500g = false;
    
    //advance function
    $scope.advance = function () {
        if (!$scope.cal0g) {
            $scope.cal0g = true;
            return;
        }
        $scope.cal500g = true;
    };

});