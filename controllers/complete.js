app.controller('complete', function ($scope, $rootScope, $interval) {

    //cancel timer
    $interval.cancel($rootScope.runTime);

    //show summary green header
    $rootScope.summaryShow = true;

    //step to final logout
    $scope.continue = function () {
        $rootScope.summaryShow = false;
        $rootScope.nextStep();
    };
});