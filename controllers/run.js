app.controller('run', function ($scope, $rootScope) {

    //active image
    $scope.activeImg = 0;

    //incrementer
    $scope.activeImgIncrement = function () {
        if ($scope.activeImg === 1) {
            $rootScope.nextStep();
            $scope.activeImg = 0;
            return;
        }
        $scope.activeImg++;
    };

});