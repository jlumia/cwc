app.directive("unitinput", function () {
    return {
        restrict: 'A',
        link: function ($scope, element) {
            element.on("click", function () {
                element.parent().parent().find("input")[0].focus();
            });
        }
    };
});

app.directive("numinput", function ($rootScope) {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            $rootScope.currentModel = null;
            element.on("focus", function () {
                $rootScope.showNumpad = true;
                $rootScope.currentModel = ngModelCtrl;
            });
        }
    };
});

app.directive('numberpad', function ($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'numpad.html',
        link: function ($scope) {
            $scope.nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
            $scope.numValue = '';
            $scope.numClick = function (n) {
                var num = n.toString();
                $scope.numValue += num;
                angular.element(document.getElementById("numInput")).html($scope.numValue);
            };

            $scope.hasDecimal = function () {
                if ($scope.numValue.indexOf(".") > -1) {
                    return true;
                }
                return false;
            };

            $scope.pos = true;
            $scope.posNeg = function () {
                $scope.pos = !$scope.pos;
                if (!$scope.pos) {
                    $scope.numValue = "-" + $scope.numValue;
                } else {
                    $scope.numValue = $scope.numValue.substring(1);
                }
                angular.element(document.getElementById("numInput")).html($scope.numValue);
            };

            $scope.numClear = function () {
                $scope.pos = true;
                $scope.numValue = '';
                angular.element(document.getElementById("numInput")).html($scope.numValue);
            };

            $scope.closeNumpad = function () {
                $rootScope.showNumpad = false;
                $scope.numClear();
            };

            $scope.getValue = function () {
                $rootScope.currentModel.$setViewValue($scope.numValue);
                $rootScope.currentModel.$render();
                $rootScope.showNumpad = false;
                $scope.numClear();
            };
        }
    };
});